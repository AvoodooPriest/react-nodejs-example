
## SSH to AWS EC2 Instance 
Local:
```shell
## SSH to AWS instance with the given user ec2-user
ssh -i ~/.ssh/aws-ec2-docker-server.pem ec2-user@35.158.95.138
```
On EC2 instance:
```shell
[ec2-user@ip-172-31-40-96 ~]$ 
sudo yum update
sudo yum install docker
docker --version
sudo service docker start
## docker command with sudo
sudo docker ps
## Start Docker service
sudo service docker start
## Add ec2-user to docker group to dismiss sudo before every docker command
sudo usermod -aG docker $USER
## shows the groups of the current user (Changes are visibil after re-login)
groups
exit
groups -> ec2-user adm wheel systemd-journal docker
## docker command without sudo
docker ps
```

## Docker Login to Nexus Repo
The Docker login without an entry in `/etc/docker/daemon.json` will fail when connecting to insecure repos (non-HTTPS) 
Nexus repo `docker-hosted` found under `64.227.118.249:8083` is such an insecure repo.
Configure insecure access by creating or amend file '/etc/docker/daemon.json' to use insecure Http connections with e.g.
```shell
{
  "insecure-registries":["64.227.118.249:8083","https://registry-1.docker.io"]
}
```

## Restart Docker Service
Restart the Docker service on your EC2 instance and re-login via SSH
```shell
## [ec2-user@ip-172-31-40-96 ~]$
sudo service docker restart
## SSH to AWS instance with the given user ec2-user
ssh -i ~/.ssh/aws-ec2-docker-server.pem ec2-user@35.158.95.138
```

## Docker Build and Push
Build the Docker image and push it to the Nexus `docker-hosted` repository previously logged-in (@See IntelliJ project `developing-with-docker`)

```shell
docker build -t 64.227.118.249:8083/demo-app:1.0 . 
## echo Whataday@DO-01 | docker login 64.227.118.249:8083 -u frank --password-stdin
## Error: Cannot perform an interactive login from a non TTY device
docker login 64.227.118.249:8083 -u frank
## password: WelcheintagbeiDO-/d/d
docker push 64.227.118.249:8083/demo-app:1.0
```
